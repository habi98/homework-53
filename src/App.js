import React, { Component } from 'react';

import AddTaskForm from './components/AddTaskForm';
import Task from './components/Task';

import './App.css';

class App extends Component {
    state = {
        currentTask : 3,
        value : '',
        tasks : [{task: 'buy milk', id:1},{task: 'buy milk', id:2},{task: 'buy milk', id:3}]
    };

    addTaskHandler = () => {
        let newTask = {task: this.state.value, id: this.state.currentTask + 1};
        let tasks = [...this.state.tasks, newTask];
        this.setState({tasks, value: '', currentTask: this.state.currentTask + 1})

    };

    removeTaskHandler = (task) => {
        let id = this.state.tasks.findIndex((item) => item.id === task);
        let tasks = [...this.state.tasks];
        tasks.splice(id, 1);
        this.setState({tasks, currentTask: this.state.currentTask -1})
    };

  inputChangeHandler = (value) => (
      this.setState({value})
  );

  render() {
     return (
         <div className="App">
           <AddTaskForm addTask={this.addTaskHandler} value={this.state.value} changed={this.inputChangeHandler}/>
             {this.state.tasks.map((task, id) =>  <Task id={task.id} removeTask={this.removeTaskHandler} text={task.task} key={id}/>)}
         </div>
    );
  }
}

export default App;
