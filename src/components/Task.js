import React from 'react';
import './Task.css';

const Task = (props) => {
    return (
          <div className="task">
             <p>{props.text}<span><button className="form-btn" onClick={() => props.removeTask(props.id)}>X</button></span></p>
          </div>

    )
};

export default Task;