import React from 'react'

import  './AddTaskForm.css';

const AddTaskForm = (props) => {
    return (
       <div className="form">
           <input onChange={(e) => props.changed(e.target.value)} value={props.value} className="task-form" type="text" placeholder="add new task"/>
           <button onClick={props.addTask} className="btn-add">Add</button>
       </div>
    )
};

export default AddTaskForm;

